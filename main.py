# Spotipy
import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyOAuth

# For config yaml
from yaml import load

# Need sleep.. zz
from time import sleep

# CSV
import csv

# Change if needed
WAIT_TIME        = 2
SCOPE            = 'playlist-read-private'
ARTIST_SEPARATOR = ', '


# Load config
def load_config():
    stream = open('config.yaml')

    try:
        from yaml import CLoader as Loader
    except ImportError:
        from yaml import Loader

    return load(stream, Loader=Loader)


# Load config
user_config = load_config()

SPOTIPY_CLIENT_ID     = user_config['client_id']
SPOTIPY_CLIENT_SECRET = user_config['client_secret']
SPOTIPY_REDIRECT_URI  = user_config['redirect_uri']

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=SCOPE, client_id=SPOTIPY_CLIENT_ID,
    client_secret=SPOTIPY_CLIENT_SECRET, redirect_uri=SPOTIPY_REDIRECT_URI))
    
if not sp:
    exit('No auth, no fun')

# Get playlists to playdict
playdicts = []
playlists = sp.current_user_playlists(limit=50, offset=0)

while playlists:
    for i, playlist in enumerate(playlists['items']):
        playdicts.append( {
            "nr"    : i + 1,
            "id"    : playlist['id'],
            "name"  : playlist['name'],
            "tracks": playlist['tracks']['total'] 
        } )

    if playlists['next']:
        playlists = sp.next(playlists)
    else:
        playlists = None


# Loop playlists and save tracks to csv file
pl_count = len(playdicts)
for pl in playdicts:
    if pl['tracks'] == 0:
        print(f'Skipping {pl["name"]} - no tracks')
        continue

    # Release Radas has no name
    # Is it possible to have no name on normal playlist? TODO: test
    if pl['name'] == '':
        pl['name'] = 'Release Radar'

    print(f'Processing: {pl["name"]} ({pl["nr"]}/{pl_count})')

    # Create a csv file / overwrite old
    with open(pl['name'] + '.csv', mode='w') as playlist_file:
        writer = csv.writer(playlist_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    
        # Write header row
        writer.writerow(['Artists','Track','Album','Release','Year','URI'])

        # Be polite and wait a bit (is this needed?)
        sleep(WAIT_TIME)

        # Get tracks
        result = sp.playlist(playlist_id=pl['id'], fields='tracks,next')
        tracks = result['tracks']
        while tracks:
            for item in tracks['items']:
                artists = ARTIST_SEPARATOR.join( [a['name'] for a in item['track']['artists'] ] )
                rel_date = item['track']['album']['release_date']
                year = ''
                if rel_date and len(rel_date) > 3:
                    year = rel_date[0:4]

                writer.writerow([artists, item['track']['name'], item['track']['album']['name'], rel_date, year, item['track']['album']['uri']])

            if 'next' in tracks and tracks['next']:
                tracks = sp.next(tracks)
            else:
                tracks = None

print('Done. Have fun!')
