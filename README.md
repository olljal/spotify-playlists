# Spotify playlists

Backup playlists to csv files.

1. Create and activate virtual env
2. Install requirements
3. Copy config.yaml.template to config.yaml
4. Set config.yaml values
5. Run main.py
